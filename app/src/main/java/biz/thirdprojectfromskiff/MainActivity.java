package biz.thirdprojectfromskiff;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.part_buttons_dialer_fragment);
        getSupportActionBar().setDisplayShowTitleEnabled(false);
    }
}
